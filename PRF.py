from pbkdf2 import PBKDF2
from util import *
import hmac, hashlib
from util import *

def compare_str(str1, str2):
    if stoi(str1) < stoi(str2):
        return (str1, str2)
    return (str2, str1)

def PRFn(key, app_str, data, n):
    assert n == 128 or n == 256 or n == 384 or n == 512, "Invalid n value."

    R = ""
    i = 0
    hashmac = hmac.new(key, digestmod=hashlib.sha1)

    while len(R) < n:
        concat = app_str + '\x00' + str(data) + str(i)
        hashmac.update(concat)
        r = hashmac.digest()
        i = i + 1
        R += r

    return R[:512].encode('hex')
