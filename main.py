from scapy.all import *
from scapy_eap import *
from pbkdf2 import *
from PRF import *

def open_files():
    paquets = rdpcap("capture.pcap")

    macBox = paquets[0].addr2
    ssid = paquets[0].info
    nonceBox = WPA_key(str(paquets[1])).nonce
    macClient = paquets[2].addr2
    nonceClient = WPA_key(str(paquets[2])).nonce


    paquet4 = rdpcap("capture.pcap")[4]
    paquet4str = str(paquet4).encode('hex')
    paquet4str = paquet4str[:-36]
    print paquet4str

    return (macBox, macClient, ssid, nonceBox, nonceClient, paquet4)

def get_tests():
    paquet4str = EAPOL('0103005ffe01090000000000000000001400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'.decode('hex'))

    return ("00:0c:41:d2:94:fb", "00:0d:3a:26:10:fb", "linksys54gh", "9e9988bde2cba74395c0289ffda07bc41ffa889a3309237a2240c934bcdc7ddb", "893ee551214557fff3c076ac977915a2060727038e9bea9b6619a5bab40f89c1", paquet4str)


def test_pwd(pwd, data):
    (macBox, macClient, ssid, nonceBox, nonceClient, paquet4str) = data
    pmk = PBKDF2(pwd, ssid).read(4096 / 8) # 4096 bits

    (lowerMac, higherMac) = compare_str(macBox, macClient)
    (lowerNonce, higherNonce) = compare_str(nonceBox, nonceClient)

    PTK = PRFn(pmk, "Pairwise key expansion", str(lowerMac) + str(higherMac) + str(lowerNonce) + str(higherNonce), 512)

    KCK = PTK[:128]
    KEK = PTK[128:256]
    TK  = PTK[256:]
    SK  = TK[:128]
    MK  = TK[128:]

    hashmac = hmac.new(KCK, digestmod=hashlib.md5)
    hashmac.update(str(paquet4str))
    print hashmac.digest().encode('hex')

def notre_algo():
    current_pwd = "aaaa"


    truc = "aab2812"
    print type(truc)
    print truc
    truc = truc.encode('hex')
    print type(truc)
    print truc
    print "gato"

    #test_pwd("aaaa" + current_pwd, open_files())
    test_pwd("radiustest", get_tests())
if __name__ == "__main__":
    notre_algo()
